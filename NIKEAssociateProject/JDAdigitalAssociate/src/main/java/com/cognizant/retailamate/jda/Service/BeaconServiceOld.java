package com.cognizant.retailamate.jda.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;


import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.cognizant.retailamate.jda.Model.AddAssociateModel;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.google.gson.Gson;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by 452781 on 11/21/2016.
 */
public class BeaconServiceOld extends Service {

    private static final String TAG = BeaconServiceOld.class.getSimpleName();
    BeaconManager beaconManager;

    Gson gson = new Gson();
    double appproxDis;

    String beaconList[] = {"b9407f30-f5f8-466e-aff9-25556b57fe6d", "10948f73-c7c5-27f2-d80d-3f7a42d78f74", "bdb230bd-0aee-dbe6-ded4-6b3117308b5e"};
    private UUID beaconUUID;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("TAG", "onBind BeaconServiceOld");
        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                Log.e("TAG", "Beacon onService Ready");
                beaconManager.startMonitoring(new Region(
                        "monitored region",
                        UUID.fromString("BDB230BD-0AEE-DBE6-DED4-6B3117308B5E"),
                        48120, 64413));
            }
        });
        beaconManager.setBackgroundScanPeriod(500, 1000);


        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Log.e("TAG", "beaconDetection");
                beaconDetection();
                handler.postDelayed(this, 10000);
            }
        }, 10000);

    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.e("TAG", "onBind BeaconServiceOld");
        return null;
    }

    public void beaconDetection() {

        AddAssociateModel addAssociateModel = new AddAssociateModel();

        addAssociateModel.setAssociateId(GlobalClass.associateId);
        addAssociateModel.setBeaconId(beaconList[randomNumber()]);
        addAssociateModel.setCaptureTime(dateNtime());
        addAssociateModel.setFirstName("John");
        addAssociateModel.setLastName("Doe");


        dateNtime();

        AssociateSignalRService.sendMessage(gson.toJson(addAssociateModel));

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, final List beacons) {
                Log.e(TAG, "Ranged beacons: " + beacons);
                Log.e(TAG, "Ranged region: " + region);
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        stopSelf();
    }

    public Integer randomNumber() {
        Random r = new Random();
        int Low = 0;
        int High = 3;
        int Result = r.nextInt(High - Low) + Low;
        return Result;
    }

    public String dateNtime() {
        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-M-dThh:mm:ss", d.getTime());
        return (String) s;
    }
}
