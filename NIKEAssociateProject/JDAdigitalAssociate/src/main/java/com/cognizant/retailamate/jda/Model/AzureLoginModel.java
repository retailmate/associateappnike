package com.cognizant.retailamate.jda.Model;

/**
 * Created by 452781 on 11/29/2016.
 */
public class AzureLoginModel {

    /**
     * userID : 1000232
     * name : {"loginName":"wfmgm","firstName":"Elaine","lastName":"Murphy","middleName":null,"nickName":null}
     */

    private int userID;
    /**
     * loginName : wfmgm
     * firstName : Elaine
     * lastName : Murphy
     * middleName : null
     * nickName : null
     */

    private NameBean name;

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public NameBean getName() {
        return name;
    }

    public void setName(NameBean name) {
        this.name = name;
    }

    public static class NameBean {
        private String loginName;
        private String firstName;
        private String lastName;
        private Object middleName;
        private Object nickName;

        public String getLoginName() {
            return loginName;
        }

        public void setLoginName(String loginName) {
            this.loginName = loginName;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public Object getMiddleName() {
            return middleName;
        }

        public void setMiddleName(Object middleName) {
            this.middleName = middleName;
        }

        public Object getNickName() {
            return nickName;
        }

        public void setNickName(Object nickName) {
            this.nickName = nickName;
        }
    }
}
