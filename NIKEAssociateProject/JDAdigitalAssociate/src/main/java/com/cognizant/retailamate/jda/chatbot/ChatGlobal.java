package com.cognizant.retailamate.jda.chatbot;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.ImageButton;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guest_User on 22/11/16.
 */

public class ChatGlobal {
    /**
     * Refactored searchAPIToken to googleToken
     */

    public ChatGlobal(Context context) {
        this.mContext = context;
    }

    public static final int SEND = 0;
    public static final int RECEIVE = 1;
    public static final int RECEIVE_PRODUCT = 2;
    public static final int RECEIVE_OFFER = 3;
    public static final int RECEIVE_SEARCH = 4;
    public static final int RECEIVE_SUGGEST = 5;
    // Base url for Luis
    public static String baseURL = "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/3af205c0-d01e-4b20-a67d-3ebdd7140791?subscription-key=665d662ecb614e40a54cf8d166989398&q=";
    //token for search api
//    public static String googleToken ="eyJhbGciOiJSUzI1NiIsImtpZCI6IjZjNzkxMWI4YWY0MGNhNmU5YjgzZWMyZTU2MTRjN2U2ZDc4MjVhMGYifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJpYXQiOjE0ODUzNDYyMTgsImV4cCI6MTQ4NTM0OTgxOCwiYXVkIjoiNjU2NTUxODMwMDYwLXVpaWptb3A3YWxvbGR1NjdlbGlpOXZoaGM4cWNuazR0LmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE3NTYzNjc1MDA4NDE4MDA2NTA3IiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF6cCI6IjY1NjU1MTgzMDA2MC1odjM2ZWthdXFrNzFkaW40bjVqNTcxcGRnMzFodHU5OS5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsImVtYWlsIjoicmV0YWlsbWF0ZWF4QGdtYWlsLmNvbSIsIm5hbWUiOiJBbWVyIEhhc2FuIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS8tMjkwbUZGX1lIaUkvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQURQbGhmS0VMd1ZZNzluWWxzdWo0M2JtYW02eUxibjVHdy9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiQW1lciIsImZhbWlseV9uYW1lIjoiSGFzYW4iLCJsb2NhbGUiOiJlbiJ9.KW3EKHAT4DYIq7e0Hufw3t1eyJYCOlxrFDN2K6Xse30iIPqb7cugh6YOyaffAUsEKHOr9W6C0O02NKJ5Pc8eH33Uq5gIrXtLo6e2K-hyddRCs1G-AjYCHlfHXvlnN4Ogp-LGZbQLBQ6fK3C1TcGBBgQ5lO3I-UjImCfmqw2pEgjPfFrU9ZSby9gDIHiiflaXBUsS062LU_UrOOydSFo31JipNfV3vTj6-dNAQM7AtHsf-2lQs5dHbuZwmv9SyInn7Ydy7equqprTGj2loDZaE6O_clTx1nhwG6ECk7pD97qgvkXlOjrqvV0gan0aZo-N15L0hZcFQDDAoW4f4qxXhw";
//    public static String googleToken = AccountState.getTokenID();
    //for YES/NO(means optional) response
    public static boolean isAsked = false;
    public static boolean askedSpecific = false;
    public static boolean intentIsRecommend = false;
    public static boolean intentIsAvailable = false;
    public static boolean intentIsPrice = false;
    public static boolean intentIsAddToCart = false;
    public static boolean isaddToCartAPI = false;
    //for mandatory response
    public static boolean isPrompt = false;
    public static String imageBaseUrl = "https://Nikedevret.cloudax.dynamics.com/mediaserver/";
    public static String recommendAPI = "http://rmnikeapiapp.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    public static String baseAddToCartAPI = "http://nikecustomervalidationax.azurewebsites.net/api/cart/CreateCart?idToken=";
    public static String baseProductSearchAPI = "https://Nikedevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=68719476778,catalogId=0,searchText=%27";
    public static boolean tapToAdd = false;
    public static StringBuffer cartBuffer = new StringBuffer();
    public static String productID = "";
    public static String userId = "004021";
    //    public static String userId = AccountState.getUserID();
    public static TextToSpeech textToSpeech;
    public static int result;
    public static Context mContext;
    public static boolean online = true;

    //FOR GOOGLE SEARCH IN DEFAULT CASE.
    //https://www.googleapis.com/customsearch/v1?q=do+you+have+dell+laptop&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ
    public static String baseSearchUrl = "https://www.googleapis.com/customsearch/v1?q=";
    public static String endSearchUrl = "&cx=006896418020272592208%3Agznxrju2_ha&filter=1&num=5&safe=medium&key=AIzaSyAPXvMXt1MR5PMYMH7hoi2Pvfx2deHz6TQ";
    public static boolean hasSearchImage = false;

    //Intent specific Info
    public static String contextId;
    public static String lastIntent = "";

    //Layout Components
    public static RecyclerView mRecyclerView;
    public static ChatAdapter mAdapter;
    public static EditText chatText;
    public static ImageButton sendButton;
    public static RecyclerView.LayoutManager mLayoutManager;

    public static List<String> mDataset;
    public static ArrayList<Integer> mDatasetTypes;

    public static List<String> entityItemList = new ArrayList<>();
    public static List<String> entityBooleanList = new ArrayList<>();
    public static List<Integer> entityNumberList = new ArrayList<>();
    public static List<String> itemMatchedList = new ArrayList<>();
    public static List<String> itemMatchedIdList = new ArrayList<>();

    public static List<ChatDataModel> chatDataModels;

    public static boolean variantNeeded = true;
    public static boolean isVariantAPI = false;

    public static int countPrompt = 0;
    public static String mSender = "Raymont Brattly";
    public static String mRecipient = "Amer Hasan";


}
