package com.cognizant.retailamate.jda.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.associatetasks.AssociateJobListActivity;

/**
 * Created by 452781 on 12/12/2016.
 */
public class DialogActivity extends AppCompatActivity {


    final float growTo = .1f;
    final long duration = 500;
    ImageView imageView;
    TextView greeting, location, title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);


        ScaleAnimation grow = new ScaleAnimation(1, growTo, 1, growTo,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(duration / 2);
        ScaleAnimation shrink = new ScaleAnimation(growTo, 1, growTo, 1,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(duration / 2);
        shrink.setStartOffset(duration / 2);
        AnimationSet growAndShrink = new AnimationSet(true);
        growAndShrink.setInterpolator(new LinearInterpolator());
        growAndShrink.addAnimation(shrink);
        imageView = (ImageView) findViewById(R.id.imageViewPic);
        imageView.startAnimation(growAndShrink);

        greeting = (TextView) findViewById(R.id.greet_name);
        location = (TextView) findViewById(R.id.location);
        title = (TextView) findViewById(R.id.title);

        String flag = getIntent().getStringExtra("type");

        if (flag.equals("greeting")) {
            title.setText("Customer Inbound");
            greeting.setText("Hey! I'm " + getIntent().getStringExtra("name"));
            location.setText("I just stepped into the store.");

        } else if (flag.equals("geo")) {

            title.setText("Geofence Alert");
            greeting.setText(getIntent().getStringExtra("name"));
            location.setText("is less than " + getIntent().getStringExtra("dist") + " from the store.");
        } else {


            AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.MyAlertDialogStyle)).create();
            alertDialog.setTitle("New Tasks Alert!");
            alertDialog.setMessage("Check new tasks assigned to you ?");
            // Alert dialog button
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Not right now",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Alert dialog action goes here
                            // onClick button code here
                            dialog.dismiss();// use dismiss to cancel alert dialog
                            finish();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(getApplicationContext(), AssociateJobListActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    finish();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
