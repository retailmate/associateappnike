package com.cognizant.retailamate.jda.manager;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.retailamate.jda.Model.AllAssociatePositionModel;
import com.cognizant.retailamate.jda.Model.AssociateDetailsModel;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.Service.ManagerSignalRService;
import com.cognizant.retailamate.jda.associate.AssociateProfileActivity;
import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.Constants;
import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;

/**
 * Created by 452781 on 10/26/2016.
 */
public class HeatMapActivity extends AppCompatActivity {

    int tempx = 720;
    int tempy = 1280;

    int touchCount = 0;
    int touchedItem;
    String directionList[] = {"NW", "SE", "NE", "SW", "E", "N", "S", "W"};
    private static final String TAG = HeatMapActivity.class.getSimpleName();
    private final Context mContext = this;
    private boolean managerBound = false;
    private ManagerSignalRService managerService;

    TextView associateNameView, associateJobView, assoociateZone, assoociateLastSeen;

    AllAssociatePositionModel[] allAssociatePositionModel;
    AssociateDetailsModel[] associateDetailsModel;
    public static boolean managerServiceReady = false;
    static Gson gson;
    ImageView[] salesPersonArrayView, salesPersonTextView;
    int[] salesPersonArrayId, salesPersonTextHolderDataId;
    boolean[] flag;
    TextView[] nameTextView;
    RelativeLayout rl;
    int Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, Ex, Ey, Fx, Fy, Gx, Gy, Hx, Hy, Ix, Iy, Jx, Jy, Kx, Ky, Lx, Ly, Mx, My, Nx, Ny, aForXAxis, bForXAxis, cForXAxis, aForYAxis, bForYAxis, cForYAxis;

    String beaconList[] = {"b9407f30-f5f8-466e-aff9-25556b57fe6d", "10948f73-c7c5-27f2-d80d-3f7a42d78f74", "bdb230bd-0aee-dbe6-ded4-6b3117308b5e"};
    String zone[] = {"Drinkware", "Serveware", "Decor"};

    int screenWidth;
    int screenHeight;
    LinearLayout linearLayout;
    final int sdk = android.os.Build.VERSION.SDK_INT;

    RelativeLayout.LayoutParams params;
    RelativeLayout.LayoutParams textHolderParams;

    LocalBroadcastManager mLocalBroadcastManager;
//    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (intent.getAction().equals("action.close")) {
//                Log.e(TAG, "on broadcast receive");
//                clearMap();
//                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//                    rl.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_zone_maked));
//                } else {
//                    rl.setBackground(getResources().getDrawable(R.drawable.layout_zone_maked));
//                }
//                generateMap(GlobalClass.getAssociateBeacon());
//            }
//        }
//    };

    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("action.close")) {
                try {
                    Log.e(TAG, "on broadcast receive");
                    clearMap();
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        rl.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_zone_maked));
                    } else {
                        rl.setBackground(getResources().getDrawable(R.drawable.layout_zone_maked));
                    }

                    HashMap<String, String> hm = new HashMap<>();

                    for (int i = 0; i < GlobalClass.allAssociatePositionModel.length; i++) {
                        int length = String.valueOf(GlobalClass.allAssociatePositionModel[i].getAssociateId()).length();
                        if (length == 7 && GlobalClass.allAssociatePositionModel[i].getBeaconId() != null) {

                            AssociateDetailsModel associateDetailsModel = new AssociateDetailsModel();
                            associateDetailsModel.setAssociateID(String.valueOf(GlobalClass.allAssociatePositionModel[i].getAssociateId()));
                            associateDetailsModel.setBeaconId(GlobalClass.allAssociatePositionModel[i].getBeaconId());
                            associateDetailsModel.setName(GlobalClass.allAssociatePositionModel[i].getFirstName() + " " + GlobalClass.allAssociatePositionModel[i].getLastName());
                            associateDetailsModel.setJob(GlobalClass.allAssociatePositionModel[i].getJobName());
                            associateDetailsModel.setTime(GlobalClass.allAssociatePositionModel[i].getStartTime().substring(GlobalClass.allAssociatePositionModel[i].getStartTime().lastIndexOf("T") + 1) + " - " + GlobalClass.allAssociatePositionModel[i].getEndTime().substring(GlobalClass.allAssociatePositionModel[i].getEndTime().lastIndexOf("T") + 1));
                            Log.e("####", gson.toJson(associateDetailsModel));
                            Log.e("####", GlobalClass.allAssociatePositionModel[i].getBeaconId());

                            int zoneArrayPosition = Arrays.asList(beaconList).indexOf(allAssociatePositionModel[i].getBeaconId());
                            System.out.println("####" + " Broadcast receiver zoneArrayPosition :" + zoneArrayPosition);

                            if (zoneArrayPosition != -1) {
                                associateDetailsModel.setZone(zone[zoneArrayPosition]);
                                hm.put(String.valueOf(GlobalClass.allAssociatePositionModel[i].getAssociateId()), gson.toJson(associateDetailsModel));

                            } else {
                                associateDetailsModel.setZone("Zone -");
                            }
//                            associateDetailsModel.setZone(zone[Arrays.asList(beaconList).indexOf(GlobalClass.allAssociatePositionModel[i].getBeaconId())]);

                        }
                    }
                    if (hm.size() > 0) {
                        generateMap(hm);
                    } else {
                        Toast.makeText(HeatMapActivity.this, "No associate has logged in yet.", Toast.LENGTH_LONG).show();

                    }
                } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                    Toast.makeText(HeatMapActivity.this, "Poor network connection. Try again.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    finish();
                }

            }
     /*   else if(intent.getAction().equals("layout.close")){
            Log.e("####","generateMap(ApplicationConstants.getAssociateBeacon()) touch");
            clearLayout();
        }*/
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.heatmap);

//        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
//        mToolBar.setNavigationIcon(R.drawable.back);
//        mToolBar.setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        mToolBar.setTitleTextColor(Color.WHITE);
//        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });


        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("action.close");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);

        linearLayout = (LinearLayout) findViewById(R.id.snackbar_layout);
        linearLayout.setVisibility(View.INVISIBLE);

        associateNameView = (TextView) findViewById(R.id.associate_name);
        associateJobView = (TextView) findViewById(R.id.associate_job);
        assoociateZone = (TextView) findViewById(R.id.associate_zone);
        assoociateLastSeen = (TextView) findViewById(R.id.associate_lastseen);

        gson = new Gson();
        measureScreen();
        beaconPositions();
        Platform.loadPlatformComponent(new AndroidPlatformComponent());


        if (AccountState.getOfflineMode()) {

            generateOfflineAssociateData();
            generateMap(GlobalClass.associateBeacon);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Log.e("TAG", "beaconDetection");
                    clearMap();
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        rl.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_zone_maked));
                    } else {
                        rl.setBackground(getResources().getDrawable(R.drawable.layout_zone_maked));
                    }
                    generateOfflineAssociateData();
                    generateMap(GlobalClass.associateBeacon);
                    handler.postDelayed(this, 20000);
                }
            }, 10000);


        } else {
            final ProgressDialog loading = ProgressDialog.show(this, "Fetching Data", "Please wait...", false, false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.getAllAssociatesURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            loading.dismiss();
                            allAssociatePositionModel = gson.fromJson(s, AllAssociatePositionModel[].class);
                            GlobalClass.allAssociatePositionModel = allAssociatePositionModel;

                      /*  if (allAssociatePositionModel.length == 0) {
                            Toast.makeText(HeatMapActivity.this, "No Associate has logged in yet.", Toast.LENGTH_LONG).show();
                        } else {*/
                            Log.e("TAG", s);
                            try {
                                for (int i = 0; i < allAssociatePositionModel.length; i++) {
                                    int length = String.valueOf(allAssociatePositionModel[i].getAssociateId()).length();
                                    if (length == 7 && allAssociatePositionModel[i].getBeaconId() != null) {

                                        AssociateDetailsModel associateDetailsModel = new AssociateDetailsModel();
                                        associateDetailsModel.setAssociateID(String.valueOf(allAssociatePositionModel[i].getAssociateId()));
                                        associateDetailsModel.setBeaconId(allAssociatePositionModel[i].getBeaconId());
                                        associateDetailsModel.setName(allAssociatePositionModel[i].getFirstName() + " " + allAssociatePositionModel[i].getLastName());
                                        associateDetailsModel.setJob(allAssociatePositionModel[i].getJobName());
                                        associateDetailsModel.setTime(allAssociatePositionModel[i].getStartTime().substring(allAssociatePositionModel[i].getStartTime().lastIndexOf("T") + 1) + " - " + allAssociatePositionModel[i].getEndTime().substring(allAssociatePositionModel[i].getEndTime().lastIndexOf("T") + 1));

                                        Log.e("####", allAssociatePositionModel[i].getBeaconId());
                                        int zoneArrayPosition = Arrays.asList(beaconList).indexOf(allAssociatePositionModel[i].getBeaconId());
                                        System.out.println("####" + "zoneArrayPosition :" + zoneArrayPosition);

                                        if (zoneArrayPosition != -1) {
                                            associateDetailsModel.setZone(zone[zoneArrayPosition]);
                                            GlobalClass.associateBeacon.put(String.valueOf(allAssociatePositionModel[i].getAssociateId()), gson.toJson(associateDetailsModel));

                                        } else {
                                            associateDetailsModel.setZone("Zone -");
                                        }

                                    }
                                }

                                if (GlobalClass.associateBeacon.size() == 0) {
                                    Toast.makeText(HeatMapActivity.this, "No Associate has logged in yet.", Toast.LENGTH_LONG).show();
                                } else {
                                    generateMap(GlobalClass.associateBeacon);
                                }
                            } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
                                Toast.makeText(HeatMapActivity.this, "Poor network connection. Try again.", Toast.LENGTH_LONG).show();
                                e.printStackTrace();
//                            finish();
                            }
                        }
//                    }//
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Dismissing the progress dialog
                            loading.dismiss();
                            Log.e("@@##", "VolleyError = " + error);
                            Log.e("@@##", "VolleyError = " + error.getMessage());
                            if (error.getMessage().contains("ConnectException") || error.getMessage().contains("UnknownHostException")) {
                                toast("Please Check Network Connection.");
                            }
                        }
                    });
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }


    protected void onDestroy() {
        super.onDestroy();
        mLocalBroadcastManager.unregisterReceiver(mBroadcastReceiver);
    }


    @Override
    protected void onStop() {
        // Unbind from the service
        if (managerBound) {
            unbindService(managerConnection);
            managerBound = false;
        }
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent managerIntent = new Intent();
                managerIntent.setClass(mContext, ManagerSignalRService.class);
                bindService(managerIntent, managerConnection, Context.BIND_AUTO_CREATE);
                Log.d(TAG, "intent to ManagerSignalRService");
            }
        }, 5000);
    }


    public void toast(String s) {
        Toast.makeText(HeatMapActivity.this, s, Toast.LENGTH_LONG).show();
    }

    public void clearMap() {
        try {
            if (rl != null) {
                rl.removeAllViews();
                for (int i = 0; i < rl.getChildCount(); i++) {
                    if (rl.getChildAt(i) instanceof TextView) {
                        rl.removeView(rl.getChildAt(i));
                    }
                    if (rl.getChildAt(i) instanceof ImageView) {
                        rl.removeView(rl.getChildAt(i));
                    }
                }
            }
        } catch (NullPointerException e) {
            Log.e(TAG, "NullPointerException on map clear");
            finish();
        }
    }


    public void generateMap(HashMap<String, String> hashMap) {
        for (Map.Entry<String, String> entry : hashMap.entrySet()) {

            Log.e(TAG, "generateMap " + entry.getKey() + ":" + entry.getValue());
        }


        String associateDetailModelString = "[";
        for (String value : hashMap.values()) {

            associateDetailModelString = associateDetailModelString + value + ",";
        }
        associateDetailModelString = associateDetailModelString.substring(0, associateDetailModelString.length() - 1) + "]";

        Log.e(TAG, "))))))))))))))" + associateDetailModelString);

        associateDetailsModel = gson.fromJson(associateDetailModelString, AssociateDetailsModel[].class);
        Log.e(TAG, associateDetailsModel[0].getBeaconId());


        salesPersonArrayView = new ImageView[associateDetailsModel.length];
        salesPersonTextView = new ImageView[associateDetailsModel.length];
        nameTextView = new TextView[associateDetailsModel.length];

        // creating id arrays for the elements used
        salesPersonArrayId = new int[associateDetailsModel.length];
        salesPersonTextHolderDataId = new int[associateDetailsModel.length];
        flag = new boolean[associateDetailsModel.length];

        for (int i = 0; i < associateDetailsModel.length; i++) {
            System.out.println("####" + associateDetailsModel[i].getBeaconId());

            System.out.println("####" + gson.toJson(associateDetailsModel));
            salesPersonArrayId[i] = i;
            salesPersonTextHolderDataId[i] = i;
            Log.e("^^^^^^^^^", "salesPersonTextHolderDataId[i] " + String.valueOf(salesPersonTextHolderDataId[i]));
            flag[i] = true;

    /*    }


        // creating and adding markers and text holders
        for (int i = 0; i < associateDetailsModel.length; i++) {*/

            int[] XYmarker = detectPointOnScreen(associateDetailsModel[i].getBeaconId(), "a", directionList[randomNumber(7)]);

            if (XYmarker[0] > tempx - 10 && XYmarker[0] < tempx + 10) {
                XYmarker[1] = XYmarker[1] + (int) (screenHeight * 0.0546875) + 10;
            }
            if (XYmarker[1] > tempy - 10 && XYmarker[1] < tempy + 10) {
                XYmarker[0] = XYmarker[0] + 20;
            }

            rl = (RelativeLayout) findViewById(R.id.relative_layout);
            nameTextView[i] = new TextView(this);
            Log.e(TAG, "nameTextView : " + String.valueOf(nameTextView.length));
            nameTextView[i].setId(salesPersonTextHolderDataId[i]);
            nameTextView[i].setTextColor(Color.WHITE);

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                nameTextView[i].setBackgroundDrawable(getResources().getDrawable(R.drawable.trans_roundedcorner_img));
            } else {
                nameTextView[i].setBackground(getResources().getDrawable(R.drawable.trans_roundedcorner_img));
            }
//            nameTextView[i].setBackground();
            if (associateDetailsModel[i].getName().length() > 15) {
                nameTextView[i].setText("     " + associateDetailsModel[i].getName().substring(0, 11) + "...");

            } else {
                nameTextView[i].setText("     " + associateDetailsModel[i].getName() + " ");
            }
            nameTextView[i].setPadding((int) (screenWidth * 0.0555555555555556), (int) (screenHeight * 0.0078125), 0, 0);
//            nameTextView[i].setTextSize((int) (screenHeight * 0.0078125));
            nameTextView[i].setTextSize(9);

//            nameTextView[i].setGravity(CENTRE);
            textHolderParams = new RelativeLayout.LayoutParams((int) (screenWidth * 0.2777777777777778), (int) (screenHeight * 0.0390625));
            textHolderParams.leftMargin = XYmarker[0] + 10;
//            + (int) (screenWidth * 0.0347222222222222);
            textHolderParams.topMargin = XYmarker[1] + 5;
//            + (int) (screenHeight * 0.003125);
            rl.addView(nameTextView[i], textHolderParams);
            nameTextView[i].setVisibility(View.VISIBLE);


            salesPersonArrayView[i] = new ImageView(this);
            Log.e(TAG, "salesPersonArrayView : " + String.valueOf(salesPersonArrayView.length));
            salesPersonArrayView[i].setId(salesPersonArrayId[i]);


//            salesPersonArrayView[i].setImageResource(R.drawable.green_pple);
            salesPersonArrayView[i].setImageResource(getImageofAssociate(associateDetailsModel[i].getAssociateID()));


            salesPersonArrayView[i].setPadding(0, 0, (int) (screenWidth * 0.0138888888888889), (int) (screenHeight * 0.0078125));
            params = new RelativeLayout.LayoutParams((int) (screenWidth * 0.0972222222222222), (int) (screenHeight * 0.0546875));

            tempy = XYmarker[1];
            tempx = XYmarker[0];
            params.leftMargin = XYmarker[0];
            params.topMargin = XYmarker[1];
            Log.e(TAG, "X: " + XYmarker[0] + " Y: " + XYmarker[1]);

            rl.addView(salesPersonArrayView[i], params);


        }

        View.OnClickListener btnListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (touchCount == 0) {
                    linearLayout.setVisibility(View.VISIBLE);
                    touchCount++;
                } else if (touchedItem == v.getId() && linearLayout.getVisibility() == View.VISIBLE) {
                    linearLayout.setVisibility(View.INVISIBLE);
                } else {
                    linearLayout.setVisibility(View.VISIBLE);

                }


                touchedItem = v.getId();


/*
                for (int i = 0; i < salesPersonArrayView.length; i++) {
                    Log.e(TAG, "v.getId() :"+v.getId());
                    Log.e(TAG, "flag[i] :"+flag[i]);

 *//*                   if (v.getId() == i && flag[i] == true) {


//                        nameTextView[i].setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                        flag[i] = false;
                    } else {
//                        nameTextView[i].setVisibility(View.INVISIBLE);
                        flag[i] = true;
                        linearLayout.setVisibility(View.INVISIBLE);
                    }*//*

                }*/
                final String s;


                Log.e("^^^^^^^", "v.getid" + String.valueOf(v.getId()));
                Log.e(TAG, "___________" + String.valueOf(associateDetailsModel.length) + "()((()()()()()");
                s = gson.toJson(associateDetailsModel[v.getId()]);
                Log.e(TAG, gson.toJson(associateDetailsModel[v.getId()]));
                associateNameView.setText(associateDetailsModel[v.getId()].getName());
                associateJobView.setText(associateDetailsModel[v.getId()].getJob());
                assoociateZone.setText(associateDetailsModel[v.getId()].getZone());
                TextView viewMore = (TextView) findViewById(R.id.view_more);


                viewMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.e(TAG, s);
                        Intent intent = new Intent(HeatMapActivity.this, AssociateProfileActivity.class);
                        intent.putExtra("associate", s);
                        intent.putExtra("manager", true);
                        startActivity(intent);

                    }
                });


         /*       Log.e("^^^^^^&^^^ touchCount", String.valueOf(touchCount));
                Log.e("^^^^^^&^^^ v.getId()", String.valueOf(v.getId()));
                if (touchCount != v.getId()) {
                    linearLayout.setVisibility(View.VISIBLE);
                } else {
                    linearLayout.setVisibility(View.INVISIBLE);
                }
                touchCount = v.getId();*/


            }
        };

        for (ImageView marker : salesPersonArrayView) {
            Log.e(TAG, "Happy");
            marker.setOnClickListener(btnListener);
        }
        for (TextView marker1 : nameTextView) {
            Log.e(TAG, "Happy");
            marker1.setOnClickListener(btnListener);
        }


    }


    public void beaconPositions() {
        Ax = (int) (screenWidth * 0.16666666666666666666666666666667); //180;
        Ay = (int) (screenHeight * 0.15625); //300;

        Bx = (int) (screenWidth * 0.46296296296296296296296296296296); //500
        By = (int) (screenHeight * 0.15625); //300;

        Cx = (int) (screenWidth * 0.77777777777777777777777777777778); //840
        Cy = (int) (screenHeight * 0.15625); //300;

        /*-------------------------------------------------------*/

        Dx = (int) (screenWidth * 0.11111111111111111111111111111111); //120
        Dy = (int) (screenHeight * 0.36458333333333333333333333333333); //700

        Ex = (int) (screenWidth * 0.33796296296296296296296296296296); //365
        Ey = (int) (screenHeight * 0.36458333333333333333333333333333); //700

        Fx = (int) (screenWidth * 0.59259259259259259259259259259259); //640
        Fy = (int) (screenHeight * 0.36458333333333333333333333333333); //700

        Gx = (int) (screenWidth * 0.85185185185185185185185185185185); //920
        Gy = (int) (screenHeight * 0.36458333333333333333333333333333); //700

        /*--------------------------------------------------------*/
        Hx = (int) (screenWidth * 0.11111111111111111111111111111111); //120
        Hy = (int) (screenHeight * 0.54166666666666666666666666666667); //1040

        Ix = (int) (screenWidth * 0.33796296296296296296296296296296); //365
        Iy = (int) (screenHeight * 0.54166666666666666666666666666667); //1040

        Jx = (int) (screenWidth * 0.59259259259259259259259259259259); //640
        Jy = (int) (screenHeight * 0.54166666666666666666666666666667); //1040

        Kx = (int) (screenWidth * 0.85185185185185185185185185185185); //920
        Ky = (int) (screenHeight * 0.54166666666666666666666666666667); //1040

        /*--------------------------------------------------------*/

        Lx = (int) (screenWidth * 0.1666666666666667); //180
        Ly = (int) (screenHeight * 0.72916666666666666666666666666667); //1400

        Mx = (int) (screenWidth * 0.46296296296296296296296296296296); //500
        My = (int) (screenHeight * 0.72916666666666666666666666666667); //1400

        Nx = (int) (screenWidth * 0.77777777777777777777777777777778); //840
        Ny = (int) (screenHeight * 0.72916666666666666666666666666667); //1400

        /*--------------------------------------------------------*/
        aForXAxis = (int) (screenWidth * 0.0902777777777778); //65
        bForXAxis = (int) (screenWidth * 0.125); //90
        cForXAxis = (int) (screenWidth * 0.1597222222222222); //115

        aForYAxis = (int) (screenHeight * 0.02734375); //35
        bForYAxis = (int) (screenHeight * 0.04296875); //55
        cForYAxis = (int) (screenHeight * 0.05078125); //65

    }

    private void measureScreen() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.widthPixels;
        screenHeight = displayMetrics.heightPixels;
        Log.e(TAG, "screenWidth=" + screenWidth + " screenHeight=" + screenHeight);
    }



    /*
    FOR DECTECING TWO POINTS ON SCREEN
     */

    private int[] detectPointOnScreen(String beaconID, String dis, String direction) {

        int[] xy = new int[2];
        int xChange = 0;
        int yChange = 0;
        int x = 0;
        int y = 0;
        int beaconX = 0;
        int beaconY = 0;

        try {
            switch (beaconID) {
                case "aaaa":
                    Log.e(TAG, ": A");
                    beaconX = Ax;
                    beaconY = Ay;
                    break;

                case "b9407f30-f5f8-466e-aff9-25556b57fe6d":

                    Log.e(TAG, ": B");

                    beaconX = Bx;
                    beaconY = By;
                    break;
                case "cccc":

                    Log.e(TAG, ": C");

                    beaconX = Cx;
                    beaconY = Cy;
                    break;

                case "10948f73-c7c5-27f2-d80d-3f7a42d78f74":

                    Log.e(TAG, ": D");

                    beaconX = Dx;
                    beaconY = Dy;
                    break;

                case "eeee":
                    Log.e(TAG, ": E");

                    beaconX = Ex;
                    beaconY = Ey;
                    break;

                case "ffff":

                    Log.e(TAG, ": F");

                    beaconX = Fx;
                    beaconY = Fy;
                    break;
                case "gggg":

                    Log.e(TAG, ": G");

                    beaconX = Gx;
                    beaconY = Gy;
                    break;

                case "hhhh":

                    Log.e(TAG, ": H");

                    beaconX = Hx;
                    beaconY = Hy;
                    break;
                case "iiii":

                    Log.e(TAG, ": I");

                    beaconX = Ix;
                    beaconY = Iy;
                    break;

                case "jjjj":

                    Log.e(TAG, ": J");

                    beaconX = Jx;
                    beaconY = Jy;
                    break;
                case "kkkk":

                    Log.e(TAG, ": K");

                    beaconX = Kx;
                    beaconY = Ky;
                    break;
                case "llll":


                    Log.e(TAG, ": L");
                    beaconX = Lx;
                    beaconY = Ly;
                    break;
                case "bdb230bd-0aee-dbe6-ded4-6b3117308b5e":
                    Log.e(TAG, ": M");
                    beaconX = Mx;
                    beaconY = My;
                    break;

                case "nnnn":
                    Log.e(TAG, ": N");
                    beaconX = Nx;
                    beaconY = Ny;
                    break;

                default:
                    Log.e(TAG, ": N");
                    beaconX = Nx;
                    beaconY = Ny;
                    break;
            }
        } catch (NullPointerException e) {

            Toast.makeText(HeatMapActivity.this, " ", Toast.LENGTH_LONG).show();

        }


        if (dis.equals("a")) {
            xChange = aForXAxis;
            yChange = aForYAxis;

        } else if (dis.equals("b")) {
            xChange = bForXAxis;
            yChange = bForYAxis;
        } else if (dis.equals("c")) {
            xChange = cForXAxis;
            yChange = cForYAxis;
        }

        if (direction.equals("NW")) {
            Log.e(TAG, "Change : NW");

            x = beaconX - xChange;
            y = beaconY - yChange;

        }
        if (direction.equals("N")) {
            Log.e(TAG, "Change : N");

            x = beaconX;
            y = beaconY - yChange;

        }
        if (direction.equals("NE")) {
            Log.e(TAG, "Change : NE");

            x = beaconX + xChange;
            y = beaconY - yChange;
        }
        if (direction.equals("W")) {
            Log.e(TAG, "Change : W");

            x = beaconX - xChange;
            y = beaconY;
        }
        if (direction.equals("E")) {
            Log.e(TAG, "Change : E");

            x = beaconX + xChange;
            y = beaconY;
        }
        if (direction.equals("SW")) {
            Log.e(TAG, "Change : SW");

            x = beaconX - xChange;
            y = beaconY + yChange;
        }
        if (direction.equals("S")) {
            Log.e(TAG, "Change : S");

            x = beaconX;
            y = beaconY + yChange;
        }
        if (direction.equals("SE")) {
            Log.e(TAG, "Change : SE");

            x = beaconX + xChange;
            y = beaconY + yChange;
        }

        xy[0] = x - randomNumber(7);
        xy[1] = y - randomNumber(7) - randomNumber(7);

//            data.add(new WeightedLatLng(x,y,(plotPoint[i].getValue()*10)));
        return xy;
    }


    public Integer randomNumber(int high) {
        Random r = new Random();
        int Low = 0;
        int High = high;
        int Result = r.nextInt(High - Low) + Low;
        return Result;
    }

    ServiceConnection managerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ManagerSignalRService.LocalBinder mBinder = (ManagerSignalRService.LocalBinder) service;
            managerService = mBinder.getService();
            managerBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.aboutusmenu, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private int getImageofAssociate(String id) {

        if (id.equals("1000243")) {
            return R.drawable.brattly;
        } else if (id.equals("1000247")) {
            return R.drawable.bush;
        } else {
            return R.drawable.callistar;
        }

    }

    /*
    OFFLINE MAP GENERATION
     */
    private void generateOfflineAssociateData() {

        AssociateDetailsModel associateDetailsModel = new AssociateDetailsModel();
        int random1 = randomNumber(3);
        associateDetailsModel.setAssociateID("1000243");
        associateDetailsModel.setBeaconId(beaconList[random1]);
        associateDetailsModel.setName("Raymont Brattly");
        associateDetailsModel.setJob("Sales Associate");
        associateDetailsModel.setTime("8:15 AM - 6:00 PM");
        associateDetailsModel.setZone(zone[random1]);

        AssociateDetailsModel associateDetailsModel1 = new AssociateDetailsModel();
        int random2 = randomNumber(3);
        associateDetailsModel1.setAssociateID("1000247");
        associateDetailsModel1.setBeaconId(beaconList[random2]);
        associateDetailsModel1.setName("Phillip Bush");
        associateDetailsModel1.setJob("Sales Associate");
        associateDetailsModel1.setTime("10:15 AM - 8:00 PM");
        associateDetailsModel1.setZone(zone[random2]);

        AssociateDetailsModel associateDetailsModel2 = new AssociateDetailsModel();
        int random3 = randomNumber(3);
        associateDetailsModel2.setAssociateID("1000375");
        associateDetailsModel2.setBeaconId(beaconList[random3]);
        associateDetailsModel2.setName("Jeffrey McCallister");
        associateDetailsModel2.setJob("Sales Associate");
        associateDetailsModel2.setTime("11:15 AM - 9:00 PM");
        associateDetailsModel2.setZone(zone[random3]);

        GlobalClass.associateBeacon.put(associateDetailsModel.getAssociateID(), gson.toJson(associateDetailsModel));
        GlobalClass.associateBeacon.put(associateDetailsModel1.getAssociateID(), gson.toJson(associateDetailsModel1));
        GlobalClass.associateBeacon.put(associateDetailsModel2.getAssociateID(), gson.toJson(associateDetailsModel2));

    }
}
