package com.cognizant.retailamate.jda.Network;

/**
 * Created by 452781 on 11/23/2016.
 */

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.BuildConfig;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * Volley adapter for JSON requests that will be parsed into Java objects by
 * Gson.
 */
public class GsonRequest<T> extends Request<T> {

    private final Gson gson = new Gson();
    private Class<T> clazz;
    private  Map<String, String> headers;
    private final Listener<T> listener;
    private JSONObject parameters = null;
    private JSONObject entityparameters = null;
    private String stringparameters = null;
    private JSONArray arrayparameters = null;




    /**
     * Make a GET request and return a parsed object from JSON.
     *  @param url     URL of the request to make
     *
     */
    public GsonRequest(int method, String url,
                       Listener<T> listener,
                       ErrorListener errorListener) {
        super(method, url, errorListener);
//        this.clazz = clazz;
//        this.headers = headers;
        this.listener = listener;
    }

    public GsonRequest(int method, String url, Class<T> clazz,
                       Map<String, String> headers, JSONObject parameters,
                       Listener<T> listener, ErrorListener errorListener) {
        this(method, url, listener, errorListener);
        this.parameters = parameters;
    }

//    public GsonRequest(int method, String url,
//                       Listener<T> listener) {
//        this(method, url, listener, errorListener);
//        this.stringparameters = stringparameters;
//    }

    public GsonRequest(int method, String url,
                       Map<String, String> headers,
                       Listener<T> listener, ErrorListener errorListener) {
        this(method, url, listener, errorListener);
        this.arrayparameters = arrayparameters;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }


    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            if (parameters != null) {
                return parameters.toString().getBytes(getParamsEncoding());
            }

            if (stringparameters != null) {
                return stringparameters.getBytes(getParamsEncoding());
            }

            if (arrayparameters != null) {
                return arrayparameters.toString().getBytes(getParamsEncoding());
            }

        } catch (UnsupportedEncodingException e) {

            Log.d("ERROR", e.toString());

        }
        return null;
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        Log.i("resp", response.statusCode + "resp2" + response.data.toString());


        try {
            String json = new String(response.data);
            logLongJsonStringIfDebuggable(json);
            Log.i("Network Response", json.trim());

            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(json);

            Map<String, String> params = new HashMap<>();
            params.put("response", json);

            if(!element.isJsonObject())
            {
                JSONObject myobject = new JSONObject(params);
                element = parser.parse(myobject.toString());
            }

            if (element.isJsonObject()) {
                return Response.success(gson.fromJson(json, clazz),
                        HttpHeaderParser.parseCacheHeaders(response));
            } else {

                Log.i("parse error", "error");

                return (Response<T>) Response.success(json,
                        HttpHeaderParser.parseCacheHeaders(response));

            }

        } catch (JsonSyntaxException e) {
            Log.i("json parse error", "json error");
            Log.e("error", e.toString());
            return Response.error(new ParseError(e));
        }


    }

    private static final int LOGCAT_MAX_LENGTH = 3950;

    public void logLongJsonStringIfDebuggable(String s) {
        if (BuildConfig.DEBUG) {
            while (s.length() > LOGCAT_MAX_LENGTH) {
                int substringIndex = s.lastIndexOf(",", LOGCAT_MAX_LENGTH);
                if (substringIndex == -1) {
                    substringIndex = LOGCAT_MAX_LENGTH;
                }
                Log.d("TAG", s.substring(0, substringIndex));
                s = s.substring(substringIndex).trim();
            }
            Log.d("TAG", s);
        }
    }

}
