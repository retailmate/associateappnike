package com.cognizant.retailamate.jda.utils;

/**
 * Created by 452781 on 12/2/2016.
 */
public class Constants {

    public static String azureLoginURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAssociateDetails?LoginName=";
    public static String getAssociateDetailsURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAssociateScheduleAPI?associateId=";
    public static String getAllAssociatesURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAllAssociatesPositionAPI";
    public static String getAssociateJobURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAssociateTasksAPI?associateId=";
    public static String updateAssociateTaskURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/UpdateTaskAPI?taskId=";

}
