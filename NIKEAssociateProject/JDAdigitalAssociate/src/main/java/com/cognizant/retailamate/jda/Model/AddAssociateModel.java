package com.cognizant.retailamate.jda.Model;

/**
 * Created by 452781 on 11/22/2016.
 */
public class AddAssociateModel {

    Integer AssociateId;
    String  BeaconId;
    String  CaptureTime;
    String  FirstName;
    String  LastName;

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public Integer getAssociateId() {
        return AssociateId;
    }

    public void setAssociateId(Integer associateId) {
        AssociateId = associateId;
    }

    public String getBeaconId() {
        return BeaconId;
    }

    public void setBeaconId(String beaconId) {
        BeaconId = beaconId;
    }

    public String getCaptureTime() {
        return CaptureTime;
    }

    public void setCaptureTime(String captureTime) {
        CaptureTime = captureTime;
    }
}
