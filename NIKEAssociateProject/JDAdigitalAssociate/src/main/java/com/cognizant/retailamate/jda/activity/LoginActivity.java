package com.cognizant.retailamate.jda.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cognizant.retailamate.jda.Model.AzureLoginModel;
import com.cognizant.retailamate.jda.Network.VolleyHelper;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.Service.AssociateSignalRService;
import com.cognizant.retailamate.jda.associate.AssociateHomeActivity;
import com.cognizant.retailamate.jda.manager.ManagerHomeActivity;
import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.Constants;
import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL;


/**
 * Created by 452781 on 11/8/2016.
 */

public class LoginActivity extends AppCompatActivity {

    private boolean isChecked = false;

    private static final String TAG = LoginActivity.class.getSimpleName();

    Gson gson = new Gson();

    EditText userNameView, passwordView;
    Button loginButton;
    ProgressDialog progress;

    private final static int LOCATION_REQUEST_CODE = 998;
    private static final int REQUEST_READ_PHONE_STATE = 111;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 123;
    private static final int REQUEST_CAMERA = 777;

    View offlineview;
    Switch offlineswitch;
    ImageView login_logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.login_activity);

        userNameView = (EditText) findViewById(R.id.userNameField);
        passwordView = (EditText) findViewById(R.id.passwordField);

        loginButton = (Button) findViewById(R.id.loginButton);


        /*

         */


        login_logo = (ImageView) findViewById(R.id.login_logo);
        offlineswitch = (Switch) findViewById(R.id.offlineswitch);


        login_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (offlineswitch.getVisibility() == View.INVISIBLE) {
                    offlineswitch.setVisibility(View.VISIBLE);
                } else {
                    offlineswitch.setVisibility(View.INVISIBLE);
                }
            }
        });

        offlineswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    offlineswitch.setChecked(true);
                    AccountState.setOfflineMode(true);
                } else {
                    offlineswitch.setChecked(false);
                    AccountState.setOfflineMode(false);

                }
            }
        });


        if (GlobalClass.checkPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {


            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (String.valueOf(userNameView.getText()).trim().equals("")) {
                        Toast.makeText(LoginActivity.this, "Please enter a valid username.", Toast.LENGTH_LONG).show();
                    } else {
                        if (AccountState.getOfflineMode()) {
                            if (String.valueOf(userNameView.getText()).trim().equalsIgnoreCase("admin")) {


                                System.out.println("@@## ADMIN OFFLINE LOGIN");
                                Intent intent = new Intent(LoginActivity.this, ManagerHomeActivity.class);
                                startActivity(intent);

                            } else {

                                System.out.println("@@## USER OFFLINE LOGIN");
                                AzureLoginModel azureLoginModel = gson.fromJson(loadLoginResponse(), AzureLoginModel.class);
//                                GlobalClass.associateUserName = String.valueOf("RBrattly");
                                GlobalClass.associateName = azureLoginModel.getName().getFirstName() + " " + azureLoginModel.getName().getLastName();
                                GlobalClass.associateId = azureLoginModel.getUserID();

                                Log.e(TAG, GlobalClass.associateName);
                                Log.e(TAG, String.valueOf(GlobalClass.associateId));

                                AccountState.setPrefUserId(String.valueOf(azureLoginModel.getUserID()));
                                System.out.println("@@## USER ID:" + AccountState.getPrefUserId());

                                Intent intent = new Intent(LoginActivity.this, AssociateHomeActivity.class);
                                startActivity(intent);
                            }
                        } else if (!AccountState.getOfflineMode()) {

//                    || String.valueOf(userNameView.getText()).trim().equals("")
                            if (!haveNetworkConnection()) {
                                Toast.makeText(LoginActivity.this, "Please check connection and try again", Toast.LENGTH_LONG).show();
                            } else {
                                Log.d(TAG, String.valueOf(userNameView.getText()));
                                if (String.valueOf(userNameView.getText()).trim().equalsIgnoreCase("admin")) {

                                    Intent intent = new Intent(LoginActivity.this, ManagerHomeActivity.class);
                                    startActivity(intent);

                                } else {
                                    GlobalClass.associateUserName = String.valueOf(userNameView.getText());
                                    progress = ProgressDialog.show(LoginActivity.this, "Connecting",
                                            "Please wait...", true);

                                    StringRequest stringRequest = new StringRequest(Request.Method.GET, Constants.azureLoginURL + GlobalClass.associateUserName,
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String s) {
                                                    try {
                                                        progress.dismiss();
                                                        AzureLoginModel azureLoginModel = gson.fromJson(s, AzureLoginModel.class);
                                                        GlobalClass.associateName = azureLoginModel.getName().getFirstName() + " " + azureLoginModel.getName().getLastName();
                                                        GlobalClass.associateId = azureLoginModel.getUserID();

                                                        Log.e(TAG, GlobalClass.associateName);
                                                        Log.e(TAG, String.valueOf(GlobalClass.associateId));

//                                                System.out.println("@@## compare" + (GlobalClass.associateId == Integer.parseInt(AccountState.getPrefUserId())));

                                                        AccountState.setPrefUserId(String.valueOf(azureLoginModel.getUserID()));
                                                        System.out.println("@@## USER ID:" + AccountState.getPrefUserId());


                                                        Intent intent = new Intent(LoginActivity.this, AssociateHomeActivity.class);
                                                        startActivity(intent);

                                                    } catch (NullPointerException | IllegalArgumentException e) {
                                                        Toast.makeText(LoginActivity.this, "Please enter valid credentials.", Toast.LENGTH_LONG).show();

                                                    } catch (IllegalStateException | JsonSyntaxException e) {
                                                        Toast.makeText(LoginActivity.this, "Invalid server response.", Toast.LENGTH_LONG).show();

                                                    }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progress.dismiss();
                                                    Toast.makeText(LoginActivity.this, "Login failed. Please enter valid credentials.", Toast.LENGTH_LONG).show();
                                                    Log.e("@@##", "VolleyError = " + error);
                                                }

                                            });
                                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    VolleyHelper.getInstance(LoginActivity.this).addToRequestQueue(stringRequest);
                                }
                            }
                        }
                    }
                }
            });


        } else

        {

            requestPermission(this, LOCATION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        offlineswitch.setChecked(AccountState.getOfflineMode());
    }

    private void requestPermission(Activity activity, int permissionRequestCode, String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            Toast.makeText(activity, "Please allow permission in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, permissionRequestCode);
        }
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        Log.e("#####", String.valueOf(haveConnectedMobile));
        Log.e("#####", String.valueOf(haveConnectedWifi));

        if (haveConnectedMobile || haveConnectedWifi) {

            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (!GlobalClass.checkPermission(this, Manifest.permission.READ_PHONE_STATE)) {
                        requestPermission(this, REQUEST_READ_PHONE_STATE, Manifest.permission.READ_PHONE_STATE);
                    }
                } else {

                    Toast.makeText(this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                }


                break;

            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO

                    if (!GlobalClass.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        requestPermission(this, REQUEST_WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    }
                }


                break;


            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    if (!GlobalClass.checkPermission(this, Manifest.permission.CAMERA)) {
                        requestPermission(this, REQUEST_CAMERA, Manifest.permission.CAMERA);
                    }
                }

                break;
            case REQUEST_CAMERA:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);

        boolean isChecked = AccountState.getOfflineMode();
        MenuItem item = menu.findItem(R.id.checkable_menu);
        item.setChecked(isChecked);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.checkable_menu) {
            isChecked = !item.isChecked();
            item.setChecked(isChecked);
            AccountState.setOfflineMode(isChecked);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
    Offline mode function to get associate details
     */
    public String loadLoginResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("AssociateLoginResponse.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}

