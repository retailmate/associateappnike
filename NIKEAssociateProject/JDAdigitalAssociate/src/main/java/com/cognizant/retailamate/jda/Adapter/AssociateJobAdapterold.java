package com.cognizant.retailamate.jda.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.cognizant.retailamate.jda.Model.AssociateJobModelold;
import com.cognizant.retailamate.jda.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by 452781 on 11/11/2016.
 */

public class AssociateJobAdapterold extends RecyclerView.Adapter<AssociateJobAdapterold.myViewHolder> {

    private final LayoutInflater inflater;

    private Context context;

    List<AssociateJobModelold.ResourceBean> dispdata= Collections.emptyList();

    public AssociateJobAdapterold(Context context, List<AssociateJobModelold.ResourceBean> dispdata){
        inflater= LayoutInflater.from(context);
        this.dispdata=dispdata;
        this.context=context;
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.associate_job_list_layout, parent, false);
        myViewHolder holder= new myViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        AssociateJobModelold.ResourceBean current= dispdata.get(position);
//        Log.e("TAG",current.getJob());
        holder.jobName.setText(current.getItemName());
        holder.jobField1.setText(current.getLocation());
        holder.jobField2.setText(current.getType());
//        holder.jobField2Count.setText(current.getQuantity());
    /*    if (current.getPriority()==1){
            holder.linearLayout.setBackgroundColor(Color.RED);

        }*/
    }

    @Override
    public int getItemCount() {
        return dispdata.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder{
        TextView jobName,jobField1,jobField2,jobField2Count;
        LinearLayout linearLayout;

        public myViewHolder(View itemView) {
            super(itemView);
            jobName= (TextView) itemView.findViewById(R.id.job_title);
            jobField1=(TextView) itemView.findViewById(R.id.job_field1);
            jobField2= (TextView) itemView.findViewById(R.id.job_field2);
            jobField2Count=(TextView) itemView.findViewById(R.id.job_field2_count);
            linearLayout= (LinearLayout) itemView.findViewById(R.id.highlight_layout);

        }
    }

}
