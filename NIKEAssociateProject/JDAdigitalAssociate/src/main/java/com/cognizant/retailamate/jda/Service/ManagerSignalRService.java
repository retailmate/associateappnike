package com.cognizant.retailamate.jda.Service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;


import com.cognizant.retailamate.jda.utils.Constants;
import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.cognizant.retailamate.jda.Model.AddAssociateModel;
import com.cognizant.retailamate.jda.Model.AddAssociatePosition;
import com.cognizant.retailamate.jda.Model.AllAssociatePositionModel;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Arrays;
import java.util.HashMap;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;

/**
 * Created by 452781 on 11/22/2016.
 */
public class ManagerSignalRService extends Service {
    private static final String TAG = ManagerSignalRService.class.getSimpleName();
    private HubConnection signalR;
    private HubProxy mHubProxy;
    MessageReceivedHandler messageReceivedHandler;
    private final IBinder mBinder = new LocalBinder(); // Binder given to clients
    Context mContext;

    public ManagerSignalRService() {
    }

    @Override
    public void onCreate() {
        mContext = this;
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        StartConnection();
        return result;
    }


    @Override
    public void onDestroy() {
        if (signalR != null) {
//            signalR.stop();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the communication channel to the service.
        StartConnection();
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public ManagerSignalRService getService() {
            return ManagerSignalRService.this;
        }
    }

    /**
     * method for clients (activities)
     */
  /*  public void sendMessage(String message) {
        String SERVER_METHOD_SEND = "Send";
        mHubProxy.invoke(SERVER_METHOD_SEND, "{ \"AssociateId\": 100535,  \"BeaconId\": \"0b3513ac-f1cf-46d7-8237-129a54c8ef7a\",  \"CaptureTime\": \"2016-11-22T07:00:00.000\"}"  );

    }*/
    private void StartConnection() {
        Platform.loadPlatformComponent(new AndroidPlatformComponent());

        Log.e(TAG, "Start Connection called.");


//        String serverUrl = "http://jdaretailmateapidev.azurewebsites.net/";
//        signalR = new HubConnection(serverUrl);
//
//        mHubProxy = signalR.createHubProxy("HubServerDev");


        String serverUrl = "http://rmnikeapiapp.azurewebsites.net/";
        signalR = new HubConnection(serverUrl);

        mHubProxy = signalR.createHubProxy("NikeHubServer");


        signalR.start().done(new Action<Void>() {
            @Override
            public void run(Void aVoid) throws Exception {
                Log.e(TAG, "^^^^ Manager Connected.");
                mHubProxy.invoke("JoinGroup", signalR.getConnectionId(), "Manager");

            }
        });
        signalR.error(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                Log.e(TAG, "onError" + throwable.getMessage());
                Log.e(TAG, "onError" + throwable.toString());
                if (throwable.getMessage().contains("ECONNRESET")) {
                    Handler handler = new Handler(Looper.getMainLooper());

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(ManagerSignalRService.this.getApplicationContext(), "Slow connection. Please restart application.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

//        signalR.received(new MessageReceivedHandler() {
//            @Override
//            public void onMessageReceived(final JsonElement jsonElement) {
//                Log.e(TAG, "onMessageReceived" + jsonElement);
//                Gson gson = new Gson();
//
//                String beaconList[] = {"b9407f30-f5f8-466e-aff9-25556b57fe6d", "10948f73-c7c5-27f2-d80d-3f7a42d78f74", "bdb230bd-0aee-dbe6-ded4-6b3117308b5e"};
//                String zone[] = {"Drinkware", "Serveware", "Decor"};
//
//
//
//                AddAssociatePosition addAssociatePosition = gson.fromJson(String.valueOf(jsonElement), AddAssociatePosition.class);
//                AddAssociateModel[] addAssociateModel = gson.fromJson(String.valueOf(addAssociatePosition.getA()), AddAssociateModel[].class);
//                AllAssociatePositionModel[] allAssociatePositionModel= GlobalClass.allAssociatePositionModel;
//
//                for (int j = 0; j <addAssociateModel.length; j++) {
//                    Log.e("####", "addAssociateModel.length "+ j +" "+addAssociateModel[j].getAssociateId());
//                    for (int i = 0; i < allAssociatePositionModel.length; i++) {
//                        Log.e("####", "allAssociatePositionModel.length " +i +" "+allAssociatePositionModel[i].getAssociateId());
//
//                        if (String.valueOf(allAssociatePositionModel[i].getAssociateId()).equals(addAssociateModel[j].getAssociateId())){
//                            Log.e("####", "Received value matched");
//
//                            AssociateDetailsModel associateDetailsModel = new AssociateDetailsModel();
//                            associateDetailsModel.setAssociateID(String.valueOf(allAssociatePositionModel[i].getAssociateId()));
//                            associateDetailsModel.setBeaconId(allAssociatePositionModel[i].getBeaconId());
//                            associateDetailsModel.setName(allAssociatePositionModel[i].getFirstName() + " " + allAssociatePositionModel[i].getLastName());
//                            associateDetailsModel.setJob(allAssociatePositionModel[i].getJobName());
//                            associateDetailsModel.setTime(allAssociatePositionModel[i].getStartTime().substring(allAssociatePositionModel[i].getStartTime().lastIndexOf("T") + 1) + " - " + allAssociatePositionModel[i].getEndTime().substring(allAssociatePositionModel[i].getEndTime().lastIndexOf("T") + 1));
//                            associateDetailsModel.setZone(zone[Arrays.asList(beaconList).indexOf(allAssociatePositionModel[i].getBeaconId())]);
//                            GlobalClass.associateBeacon.put(String.valueOf(allAssociatePositionModel[i].getAssociateId()), gson.toJson(associateDetailsModel));
//                            Log.e("####", "Received value size" + GlobalClass.associateBeacon.size());
//
//                        }
//
//                    }
//
//                }
//
//
//
//
//
//
//
////                GlobalClass.associateBeacon.put(String.valueOf(addAssociateModel[0].getAssociateId()), gson.toJson(associateDetailsModel1));
//
//
//                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
//                        .getInstance(ManagerSignalRService.this);
//                Intent intent = new Intent("action.close");
//                sendBroadcast(intent);
//                localBroadcastManager.sendBroadcast(intent);
//            }
//        });
//
//
//    }

        signalR.received(new MessageReceivedHandler() {
            @Override
            public void onMessageReceived(final JsonElement jsonElement) {
                Log.e(TAG, "onManagerMessageReceived" + jsonElement);
                Gson gson = new Gson();


                String beaconList[] = {"b9407f30-f5f8-466e-aff9-25556b57fe6d", "10948f73-c7c5-27f2-d80d-3f7a42d78f74", "bdb230bd-0aee-dbe6-ded4-6b3117308b5e"};
                String zone[] = {"Drinkware", "Serveware", "Decor"};


                AddAssociatePosition addAssociatePosition = gson.fromJson(String.valueOf(jsonElement), AddAssociatePosition.class);

                if (String.valueOf(addAssociatePosition.getM()).equals("BroadCastAssociatePosition")) {

                    Log.e(TAG, "Received manager message");

                    AddAssociateModel[] addAssociateModel = gson.fromJson(String.valueOf(addAssociatePosition.getA()), AddAssociateModel[].class);
                    AllAssociatePositionModel[] allAssociatePositionModel = GlobalClass.allAssociatePositionModel;


                    String id = "";
                    String dataString = "";
                    HashMap<String, String> hm = GlobalClass.getAssociateBeacon();
                    boolean flag = false;


                    Log.e("____", addAssociateModel[0].getAssociateId() + "   " + addAssociateModel[0].getBeaconId());

                    for (int j = 0; j < addAssociateModel.length; j++) {
                        Log.e("****", "Received associate ID : " + addAssociateModel[j].getAssociateId());
                        Log.e("####", "addAssociateModel.length " + j + " " + addAssociateModel[j].getAssociateId());
                        for (int i = 0; i < allAssociatePositionModel.length; i++) {
                            Log.e("####", "Received value not matched");
                            Log.e("****--------", "Iterated associate ID : " + allAssociatePositionModel[i].getAssociateId());

                            if (allAssociatePositionModel[i].getAssociateId() == addAssociateModel[j].getAssociateId()) {
                                Log.e("####", "Received value matched");

                                allAssociatePositionModel[i].setBeaconId(addAssociateModel[j].getBeaconId());


                            }

                        }

                    }
                    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                            .getInstance(ManagerSignalRService.this);
                    Intent intent = new Intent("action.close");
                    sendBroadcast(intent);
                    localBroadcastManager.sendBroadcast(intent);
                }
            }
        });
    }

}