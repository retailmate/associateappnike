package com.cognizant.retailamate.jda.Service;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.IBinder;

import android.support.v4.app.NotificationManagerCompat;
import android.text.format.DateFormat;
import android.util.Log;


import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.cognizant.retailamate.jda.Model.AddAssociateModel;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * Created by 452781 on 12/2/2016.
 */
public class BeaconService extends Service {


    String _iBeaconUUID = null;
    double _proxDistance;

    Gson gson = new Gson();
    double _prevProximityDistance = 0;
    // IBeaconManager iBeaconManager;

    /*
     *
     * NEW
     */
    int callCount = 0;
    Boolean customerInBoundState = false;
    private static final String TAG = "BeaconService";
    private BeaconManager beaconManager;
    private Region region;

    CountDownTimer timer;
    private static final double enterThreshold = 5;
    private static final double exitThreshold = 30;
    ArrayList<HashMap<String, Object>> history = new ArrayList<>();
    NotificationManagerCompat notificationManager;

    final static String GROUP_KEY_OFFERS = "group_key_offers";

    @Override
    public void onCreate() {
        Log.d("@@##", "serviceInit");

        notificationManager = NotificationManagerCompat.from(this);
        // serviceInit();
        Log.i(TAG, "Service onCreate");
        beaconManager = new BeaconManager(this);
//        beaconManager = AccountState.getBeaconManager();
        Log.i(TAG, "beaconManager = " + beaconManager);

    }

    private void serviceInit() {
        // iBeaconManager = IBeaconManager.getInstanceForApplication(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        // iBeaconManager.unBind(this);
        beaconManager.disconnect();

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service onStartCommand");
        beaconManager = new BeaconManager(this);
        startRanging();

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(region);
            }
        });

        return Service.START_STICKY;
    }

    private void startRanging() {

        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {

                    Beacon nearestBeacon = list.get(0);
                    System.out.println("@@##  RANGING BEACON" + nearestBeacon.getProximityUUID().toString());


					/*
                     * new
					 */


                    _iBeaconUUID = nearestBeacon.getProximityUUID().toString();
                    _proxDistance = (double) Math.round(Utils
                            .computeAccuracy(nearestBeacon) * 100) / 100;


//                    System.out.println("@@## AccountState.getPrefBeaconId()" + AccountState.getPrefBeaconId());

                    try {
                        if (_proxDistance < enterThreshold) {
                            if (AccountState.getPrefBeaconId().equals("id_token") || !AccountState.getPrefBeaconId().equals(nearestBeacon.getProximityUUID().toString())) {
                                beaconDetection(nearestBeacon);
                                AccountState.setPrefBeaconId(nearestBeacon.getProximityUUID().toString());
                            }
                        } else if (_proxDistance > exitThreshold) {
                        }
                    } catch (NullPointerException e) {

                    }

                }
            }

        });

        region = new Region("ranged region", null, null, null);
    }

    public void beaconDetection(Beacon beaconID) {

        System.out.println("@@##  NEAREST BEACON" + beaconID.getProximityUUID().toString());

        AddAssociateModel addAssociateModel = new AddAssociateModel();


        System.out.println("@@## AccountState userid" + AccountState.getPrefUserId());
//        addAssociateModel.setAssociateId(Integer.parseInt(AccountState.getPrefUserId()));
        addAssociateModel.setAssociateId(Integer.parseInt(AccountState.getPrefUserId()));
        addAssociateModel.setBeaconId(beaconID.getProximityUUID().toString());
        addAssociateModel.setCaptureTime(dateNtime());
//        addAssociateModel.setFirstName("John");
//        addAssociateModel.setLastName("Doe");


        dateNtime();

        AssociateSignalRService.sendMessage(gson.toJson(addAssociateModel));


    }

    public String dateNtime() {
        Date d = new Date();
        CharSequence s = DateFormat.format("yyyy-M-dThh:mm:ss", d.getTime());
        return (String) s;
    }


}
