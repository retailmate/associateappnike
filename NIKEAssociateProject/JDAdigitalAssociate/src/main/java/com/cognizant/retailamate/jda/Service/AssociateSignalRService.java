package com.cognizant.retailamate.jda.Service;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.cognizant.retailamate.jda.Model.AddAssociateModel;
import com.cognizant.retailamate.jda.Model.AddAssociatePosition;
import com.cognizant.retailamate.jda.Model.AssociateSignalRJobCallModel;
import com.cognizant.retailamate.jda.Model.AssociateSignalRJobCallOrderAssociateListModel;
import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.activity.DialogActivity;
import com.cognizant.retailamate.jda.activity.LoginActivity;
import com.cognizant.retailamate.jda.activity.SplashActivity;
import com.cognizant.retailamate.jda.associate.AssociateHomeActivity;
import com.cognizant.retailamate.jda.associatetasks.AssociateJobListActivity;
import com.cognizant.retailamate.jda.utils.AccountState;
import com.cognizant.retailamate.jda.utils.Constants;
import com.cognizant.retailamate.jda.utils.GlobalClass;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.MessageReceivedHandler;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.http.BasicAuthenticationCredentials;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;

import static microsoft.aspnet.signalr.client.http.BasicAuthenticationCredentials.*;

/**
 * Created by 452781 on 10/21/2016.
 */
public class AssociateSignalRService extends Service {

    private static final String TAG = "AssociateSignalRService";
    Context context;
    private HubConnection signalR;
    private static HubProxy mHubProxy;
    MessageReceivedHandler messageReceivedHandler;
    private final IBinder mBinder = new LocalBinder(); // Binder given to clients
    Boolean foreground = false;
    String type = "";
    String geo = "";

    public AssociateSignalRService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        StartConnection();
        return result;
    }


    @Override
    public void onDestroy() {
        if (signalR != null) {
            try {
                signalR.stop();
            } catch (NullPointerException e) {
                Log.e("TAG", "Cannot stop service while starting");
            }


        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the communication channel to the service.
        StartConnection();
        return mBinder;
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public AssociateSignalRService getService() {
            return AssociateSignalRService.this;
        }
    }

    /**
     * method for clients (activities)
     */
//    public static void sendMessage(String message) {
//        String SERVER_METHOD_SEND = "Send";
//        if (mHubProxy != null) {
//            mHubProxy.invoke(SERVER_METHOD_SEND, message);
//        }
//// "{ \"AssociateId\": 100535,  \"BeaconId\": \"0b3513ac-f1cf-46d7-8237-129a54c8ef7a\",  \"CaptureTime\": \"2016-11-22T07:00:00.000\"}"
//
//    }
    public static void sendMessage(String message) {
        String SERVER_METHOD_SEND = "SendAssociatePosition";
        if (mHubProxy != null) {
            mHubProxy.invoke(SERVER_METHOD_SEND, message);
        }
    }

    private void StartConnection() {
        Platform.loadPlatformComponent(new AndroidPlatformComponent());

        Log.e("TAG", "Start Connection called.");


//        String serverUrl = "http://jdaretailmateapidev.azurewebsites.net";
//        signalR = new HubConnection(serverUrl);
//        mHubProxy = signalR.createHubProxy("HubServerDev");

        String serverUrl = "http://rmnikeapiapp.azurewebsites.net/";
        signalR = new HubConnection(serverUrl);

        mHubProxy = signalR.createHubProxy("NikeHubServer");


        signalR.setMessageId("2");

//        signalR.getHeaders().put("UserRole", "2");
//        signalR.setCredentials(new BasicAuthenticationCredentials("UserRole", "2", new Base64Encoder() {
//            @Override
//            public String encodeBytes(byte[] bytes) {
//                return bytes.toString();
//            }
//        }));
//        signalR.start().done(new Action<Void>() {
//            @Override
//            public void run(Void aVoid) throws Exception {
//                Log.e("TAG", "^^^^Associate Connected.");
//                SplashActivity.associateServiceReady = true;
//            }
//        });
//        signalR.getHeaders().put("","");


        signalR.start().done(new Action<Void>() {
            @Override
            public void run(Void aVoid) throws Exception {
                Log.e(TAG, "^^^^ Associate Connected.");
                mHubProxy.invoke("JoinGroup", signalR.getConnectionId(), "Associate");
            }
        });


        mHubProxy.on("broadcastMessage", new SubscriptionHandler1<String>() {
            @Override
            public void run(String msg) {
                Log.d("result := ", msg);
            }
        }, String.class);


        signalR.error(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                Log.e("TAG", "#@#@" + throwable.getMessage());
                if (throwable.getMessage().contains("Disconnected")) {
                    signalR.stop();
                }
            }
        });

//        mHubProxy.on( "broadcastMessage", new SubscriptionHandler1<String>() {
//            @Override
//            public void run(String msg) {
//                Log.e("TAG","Broadcast Message:="+ msg);
//            }
//        }, String.class);

        signalR.received(new MessageReceivedHandler() {
            @Override
            public void onMessageReceived(final JsonElement jsonElement) {
                Log.e("TAG", "onAssociateMessageReceived" + jsonElement);

                Gson gson = new Gson();
                AddAssociatePosition getassociatedatafromsignalR = gson.fromJson(String.valueOf(jsonElement), AddAssociatePosition.class);

                if (String.valueOf(getassociatedatafromsignalR.getM()).equals("BroadCastOrderDetails")) {

                    Log.e("****", "Received associate message ");
                    AssociateSignalRJobCallOrderAssociateListModel[] associateSignalRJobCallOrderAssociateListModel = gson.fromJson(String.valueOf(getassociatedatafromsignalR.getA()), AssociateSignalRJobCallOrderAssociateListModel[].class);


                    if (checkForAssociateTask(associateSignalRJobCallOrderAssociateListModel[0].getTaskAssignedTo())) {
                        Log.e("****", "Received associate message for associate ID " + AccountState.getPrefUserId());
                        new CheckIfForeground().execute();
                    }

                } else if (String.valueOf(getassociatedatafromsignalR.getM()).equals("BroadCastCustomerGreeting")) {
                    type = "greeting";

                    new CheckIfForeground().execute();

                } else if (String.valueOf(getassociatedatafromsignalR.getM()).equals("BroadCastCustomerDistance")) {
                    type = "geo";
                    geo = "1000 m";
                    new CheckIfForeground().execute();

                }


            }
        });


    }


    private boolean checkForAssociateTask(ArrayList<AssociateSignalRJobCallModel> associateSignalRJobCallModels) {
        Log.e("****", "checkForAssociateTask called ");
        for (int i = 0; i < associateSignalRJobCallModels.size(); i++) {
            if (associateSignalRJobCallModels.get(i).getAssocID().equals(AccountState.getPrefUserId())) {
                return true;
            }
        }
        return false;
    }


    /*
    Class to check activity is running in foreground or background
     */
    private class CheckIfForeground extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {

            ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    Log.i("Foreground App", appProcess.processName);

                    if (getApplicationContext().getPackageName().equalsIgnoreCase(appProcess.processName)) {

                        foreground = true;
                        // close_app();
                    } else {
                        foreground = false;
                    }
                }
            }

            if (foreground) {

                showAlertDialog();


            } else {
                //if not foreground

                sendNotification();
            }

            return null;
        }
    }

    /*
    Create alert window
     */
    public void showAlertDialog() {

        Intent intent = new Intent(context, DialogActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("name", "Amer Hasan");
        intent.putExtra("dist", geo);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
//        new Handler(Looper.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//
//
//            }
//        });
    }

    /*

    create system notification
     */
    public void sendNotification() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this);

//Create the intent that’ll fire when the user taps the notification//

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mBuilder.setContentIntent(pendingIntent);

        mBuilder.setSmallIcon(R.mipmap.retailmate_appicon);
        mBuilder.setContentTitle("New Tasks Alert");
        mBuilder.setContentText("You have been assigned new tasks, please login to check!");

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }
}